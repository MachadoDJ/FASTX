#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# split_fasta.py
# Splits large fasta files in chunks with a maximum defined number of reads

# Import modules and libraries
import argparse,sys
from Bio import SeqIO

# Set arguments
parser=argparse.ArgumentParser()
parser.add_argument("-i","--input",help="Input file in FASTA format",type=str,required=True)
parser.add_argument("-s","--sequences",help="Maximum number of sequences per chunks",type=int,default=10000,required=False)
parser.add_argument("-b","--bases",help="Maximum number of base pair per chunks",type=int,default=10000000,required=False)
parser.add_argument("-o","--output",help="Prefix for output file names",type=str,default="output",required=False)
parser.add_argument("-v","--verbose",help="Increases output verbosity",action="store_true",default=False,required=False)
args=parser.parse_args()

# Define functions
def main():
	sys.stdout.write("Spliting {} in chunks of {} sequences (maximum {} base pairs)\n".format(args.input,args.sequences,args.bases))
	seqs=0
	nucs=0
	file=""
	count=0
	with open(args.input,"r") as handle:
		for record in SeqIO.parse(handle,"fasta"):
			seqs+=1
			nucs+=len(record.seq)
			header = str(record.description).strip()
			sequence = str(record.seq).strip()
			entry = ">{}\n{}\n".format(header, sequence)
			file+=entry
			if(nucs>=args.bases)or(seqs>=(args.sequences-1)):
				count+=1
				output_filename="{}_{}.fasta".format(args.output,count)
				handle=open(output_filename,"w")
				handle.write(file)
				handle.close()
				sys.stdout.write("File {}, {} sequences, {} nucleotides\n".format(output_filename,seqs-1,nucs-len(record.seq)))
				seqs=1
				nucs=len(record.seq)
				file=""
	if len(file) > 0:
		count+=1
		output_filename="{}_{}.fasta".format(args.output,count)
		handle=open(output_filename,"w")
		handle.write(file)
		handle.close()
		sys.stdout.write("File {}, {} sequences, {} nucleotides\n".format(output_filename,seqs,nucs))
	return

# Execute functions
main()

# Quit
exit()
