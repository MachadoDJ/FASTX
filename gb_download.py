import argparse, os
from Bio import Entrez
from Bio import SeqIO

class GenbankDownloader:
	def __init__(self, email, input_file):
		"""
		Initializes the GenbankDownloader with an email and input file.
		Email is required for using NCBI's Entrez.
		"""
		self.email = email
		self.input_file = input_file
		Entrez.email = self.email  # Set the email for Entrez

	def read_accessions(self):
		"""
		Reads accession numbers from the input file.
		Returns a list of accession numbers.
		"""
		try:
			with open(self.input_file, 'r') as f:
				accessions = [line.strip() for line in f if line.strip()]
			return accessions
		except FileNotFoundError:
			print(f"Error: The file {self.input_file} was not found.")
			return []

	def download_genbank(self, accession):
		"""
		Downloads the Genbank record for the given accession number and saves it to a .gb file.
		"""
		try:
			# Use Entrez to fetch the GenBank file in GenBank format
			print(f"Fetching Genbank file for accession: {accession}")
			with Entrez.efetch(db="nucleotide", id=accession, rettype="gb", retmode="text") as handle:
				record = SeqIO.read(handle, "genbank")
				file_name = f"{accession}.gb"
				
				# Save the GenBank record as a .gb file
				with open(file_name, 'w') as gb_file:
					SeqIO.write(record, gb_file, "genbank")
				
				print(f"Saved Genbank file for {accession} as {file_name}")
		except Exception as e:
			print(f"Error downloading {accession}: {str(e)}")

	def run(self):
		"""
		Reads the accession numbers from the input file and downloads the corresponding Genbank files.
		"""
		accessions = self.read_accessions()
		if not accessions:
			print("No accession numbers to process.")
			return

		for accession in accessions:
			self.download_genbank(accession)


def main():
	# Argument parsing
	parser = argparse.ArgumentParser(description='Download Genbank files for accession numbers.')
	parser.add_argument('-i', '--input_file', required=True, help='Input text file containing one accession number per line.')
	parser.add_argument('-e', '--email', required=True, help='Your email address (required for NCBI Entrez API).')

	# Parse the arguments
	args = parser.parse_args()

	# Create an instance of GenbankDownloader and start the download process
	downloader = GenbankDownloader(args.email, args.input_file)
	downloader.run()


if __name__ == "__main__":
	main()