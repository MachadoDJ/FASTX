#!/usr/bin/env python
# -*- coding: utf-8 -*-

# fq2fa.py
# Convert fastq files to fasta format

# Import modules and libraries
import argparse,re,sys

from Bio import SeqIO

# Set arguments
parser=argparse.ArgumentParser()
parser.add_argument("-i","--input",help="List of files in gb format",type=str,nargs="+",required=True)
args=parser.parse_args()

# Define functions
def main(prefix):
	for record in SeqIO.parse("{}.gb".format(prefix),"gb"):
		entry=">{}\n{}\n".format(prefix,record.seq)
		handle=open("{}.fasta".format(prefix),"w")
		handle.write(entry)
		handle.close()
	return


# Execute functions
for filename in args.input:
	prefix=re.compile("(.+)\.gb").findall(filename)[0]
	main(prefix)

# Quit
exit()
