#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# test_fasta_equality.py.py
# Compares two fasta files and check if sequences are the same

# Import modules and libraries
import argparse,re,sys

# Set arguments
parser=argparse.ArgumentParser()
parser.add_argument("-i1","--input1",help="File 1",type=str,required=True)
parser.add_argument("-i2","--input2",help="File 2",type=str,required=True)
args=parser.parse_args()

# Define functions
def main():
	fa1=getSequences(args.input1)
	fa2=getSequences(args.input2)
	if(fa1==fa2):
		sys.stdout.write("{} == {}\n".format(args.input1,args.input2))
	else:
		sys.stderr.write("{} and {} have different sequences\n".format(args.input1,args.input2))
	return

def getSequences(file):
	list=[]
	inputf=open(file,"r")
	fasta=inputf.read()
	inputf.read()
	for seq in re.compile(">[^\n]+\n([^>]+)",re.M|re.S).findall(fasta):
		seq=re.sub("\s","",seq).upper()
		list+=[seq]
	return sorted(list)

# Execute functions
main()


# Quit
exit()
