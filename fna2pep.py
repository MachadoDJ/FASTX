#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# fna2pep
# Reads a multi-FASTA file with protein-coding nucleotide sequences and returns a multi-FASTA file with amino acid sequences.

##
# LIBRARIES
##

import argparse, re, sys
from Bio import SeqIO
from Bio.Seq import Seq

##
# FUNCTIONS
##

def printo(var): # This is a function to print data to the standard output
	return sys.stdout.write(str(var).strip() + "\n")

def printe(var): # This is a function to print data to the standard error
	return sys.stderr.write(str(var).strip() + "\n")

def main(args): # This is the beginning of the definition of the main function
	counter = 0
	with open(args.fasta, "r") as handle: # This is an example while loop to read a FASTA file using BioPython
		for record in SeqIO.parse(handle, "fasta"):
			counter += 1
			myNucs = str(record.seq)
			if len(myNucs) % 3 == 0:
				frame = "1"
				myAminF = str(Seq(myNucs).translate(to_stop = True, table = args.table))
				myAminR = str(Seq(myNucs[::-1]).translate(to_stop = True, table = args.table))
			elif len(myNucs) % 3 == 1:
				frame = "2"
				myAminF = str(Seq(myNucs[1:]).translate(to_stop = True, table = args.table))
				myAminR = str(Seq(myNucs[1:][::-1]).translate(to_stop = True, table = args.table))
			elif len(myNucs) % 3 == 2:
				frame = "3"
				myAminF = str(Seq(myNucs[2:]).translate(to_stop = True, table = args.table))
				myAminR = str(Seq(myNucs[2:][::-1]).translate(to_stop = True, table = args.table))
			if len(myAminF) > len(myAminR):
				myAmin = myAminF
				orientation = "forward"
			else:
				myAmin = myAminR
				orientation = "reverse"
			printo(">{} frame={} orientation={}\n{}".format(record.description, frame, orientation, myAmin))
	return

##
# INITIALIZATION
##

if __name__ == '__main__':
	# The arguments are defined in the next few lines
	parser=argparse.ArgumentParser()
	parser.add_argument("-f", "--fasta", help = "FASTA file", type = str)
	parser.add_argument("-t", "--table", help = "Translation table (default = 1)", type = int, default = 1, required = False)
	# parser.add_argument("-v", "--verbose", help = "Increases output verbosity", action = "store_true")
	args = parser.parse_args()
	main(args) # This will call the main fuction

exit() # Quit this script
