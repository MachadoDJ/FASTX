#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys,re
from itertools import islice
sys.path.append("/home/dmachado/sw/PYTHON_MODULES/argparse-1.4.0")
import argparse

# Set arguments
parser=argparse.ArgumentParser()
parser.add_argument("-i","--input",help="Input file in FASTQ format",type=str,nargs="+",required=True)
parser.add_argument("-c","--check",help="Only counts reads with problems",action="store_true",default=False,required=False)
args=parser.parse_args()

# Define functions
def complete(input):
	sys.stderr.write("Editing %s ...\n"%(input))
	count=0
	record=0
	name=re.compile("[^/]+\.fastq").findall(input)[0]
	output=open("%s.mod"%(name),"a")
	with open(input,"r") as f:
		while True:
			next_4_lines=list(islice(f,4))
			if not next_4_lines:
				break
			else:
				record+=1
				if(len(next_4_lines[1])!=len(next_4_lines[3])):
					count+=1
					diff=len(next_4_lines[1])-len(next_4_lines[3])
					next_4_lines[3]="%s%s\n"%(next_4_lines[3].strip(),diff*next_4_lines[3].strip()[-1])
					#sys.stderr.write("%d : %d\n"%(record,count))
				for line in next_4_lines:
					#sys.stdout.write("%s"%(line))
					output.write(line)
	output.close()
	sys.stderr.write("TOTAL = %d\n"%(count))
	return

def check(input):
	sys.stderr.write("Checking %s ...\n"%(input))
	count=0
	record=0
	with open(input,"r") as f:
		while True:
			next_4_lines=list(islice(f,4))
			if not next_4_lines:
				break
			else:
				record+=1
				if(len(next_4_lines[1])!=len(next_4_lines[3])):
					count+=1
					sys.stderr.write("%d : %d\n"%(record,count))
	sys.stderr.write("TOTAL = %d (%s)"%(count,input))
	return


# Execute functions
for input in args.input:
	if(args.check):
		check(input)
	else:
		complete(input)

# Quit
exit()
