#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# newPythonProgram.py
# This script can be used to create new Python script from Denis' template

##
# Import libraries
##

import argparse

##
# Define functions
##

def main():
	if(args.verbose):
		print("Name = {}\nDescription = {}".format(args.name, args.description))
	template = """#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# {0}
# {1}

##
# Import libraries
##

import argparse, sys, re
from Bio import SeqIO
from Bio.Seq import Seq

##
# Define functions
##

def stdout(text):
	return sys.stdout.write("{{}}\\n".format(str(text)))

def stderr(text):
	return sys.stderr.write("{{}}\\n".format(str(text)))

def main():
	with open(args.input) as handle:
		for record in SeqIO.parse(handle, "fasta"):
			mySeq = Seq(str(record.seq))
			stdout(mySeq.complement())
	return

##
# Execute functions
##

if __name__ == '__main__':
	parser=argparse.ArgumentParser()
	parser.add_argument("-i", "--input", help = "String", type = str)
	parser.add_argument("-n", "--number", help = "Integer", type = int)
	parser.add_argument("-f", "--float", help = "Float", type = float)
	parser.add_argument("-v", "--verbose", help = "Increases output verbosity", action = "store_true")
	args = parser.parse_args()
	main() # This is the main fuction

exit() # Quit this script
""".format(args.name, args.description)
	if(args.verbose):
		print("\n---\n\n{}\n----\n".format(template))
	handle = open("{}.py".format(args.name), "w")
	handle.write(template)
	handle.close()
	if(args.verbose):
		print("The new template was written into {}.py".format(args.name))
	return

##
# Execute functions
##
if __name__ == '__main__':
	parser=argparse.ArgumentParser()
	parser.add_argument("-n", "--name", help = "What is the name of your new script? The new script will have this name plus the .py suffix.", type = str, default = "new.py")
	parser.add_argument("-d", "--description", help = "What does your new script do?", type = str, default = "This Python3 script. Use argument option -h or --help for more details.")
	parser.add_argument("-v", "--verbose", help = "Increases output verbosity", action = "store_true")
	args = parser.parse_args()
	main() # This is the main fuction

exit() # Quit this script
