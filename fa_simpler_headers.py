#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fa_simpler_headers.py
# Creates a copy of the input fasta file, replacing the sequence IDs.

# Import modules and libraries
import argparse, re, sys, os.path

try:
	from Bio import SeqIO
except:
	sys.stderr.write("! ERROR: The script fa_simpler_headers.py requires Biopython, but it could not be loaded. See https://biopython.org.\n")
	exit()

# Set arguments
parser = argparse.ArgumentParser()
parser.add_argument("-i", "--input", help = "(Multi)fasta file", type = str, required = True)
parser.add_argument("-o", "--output", help = "Prefix for output file names (default = output).", type = str, default = "output", required = False)
parser.add_argument("-p", "--prefix", help = "Prefix for sequence IDs (default = id).", type = str, default = "id", required = False)
args = parser.parse_args()

# Define functions
def main(inp, out, pre):
	syn = open("{}.tsv".format(out), "w")
	fas = open("{}.fasta".format(out), "w")
	i = 0
	for record in SeqIO.parse(inp, "fasta"):
		i += 1
		new = "{}_{}_".format(pre, i)
		old = record.description
		fas.write(">{}\n{}\n".format(new, str(record.seq)))
		syn.write("{}\t{}\n".format(old, new))
	syn.close()
	return

# Execute functions
inp = args.input
out = args.output
pre = args.prefix
if (not os.path.isfile(inp)):
	sys.stderr.write("! ERROR: Could not find file {}.\n".format(inp))
	exit()
elif (os.path.isfile("{}.tsv".format(out))):
	sys.stderr.write("! ERROR: The file {}.tsv already exists.\n".format(out))
	exit()
elif (os.path.isfile("{}.fasta".format(out))):
	sys.stderr.write("! ERROR: The file {}.fasta already exists.\n".format(out))
	exit()
else:
	main(inp, out, pre)


# Quit
exit()
