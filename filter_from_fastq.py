#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# filter_from_outfmt6.py.py
# Extract query IDs from BlastN (outfmt 6 format) into a new fastq file

# Import modules and libraries
import argparse,re

# Set arguments
parser=argparse.ArgumentParser()
parser.add_argument("-f","--fastq",dest="fq",help="Input sequence file in FASTQ format",type=str,required=True)
parser.add_argument("-s","--selected",dest="se",help="Text files with selected ids, one per line",type=str,required=True)
parser.add_argument("-o","--output",dest="out",help="Output file name",type=str,required=True)
args=parser.parse_args()

# Define functions
def main():
	q=parse_list(args.se)
	parse_fq(args.fq,q)
	return

def parse_list(se):
	q=[]
	handle=open(se,"r")
	for l in handle.readlines():
		l=l.strip()
		if(l):
			q+=[l]
	handle.close()
	return q

def parse_fq(f,q):
	output=open(args.out,"w")
	output.close()
	output=open(args.out,"a")
	input=open(f,"r")
	while True:
		l1=input.readline().strip()
		l2=input.readline().strip()
		l3=input.readline().strip()
		l4=input.readline().strip()
		if not l4:
			break
		else:
			if(l1 in q):
				entry="{}\n{}\n{}\n{}\n".format(l1,l2,l3,l4)
				output.write(entry)
	output.close()
	input.close()
	return

# Execute functions
main()


# Quit
exit()
