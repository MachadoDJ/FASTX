#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fq_checker.py
# Receives a list of FASTQ files, return first corrupted reads

# Import modules and libraries
import argparse, re, os, sys
from itertools import islice

# Set arguments
parser = argparse.ArgumentParser()
parser.add_argument("-i", "--input",  help = "FASTQ files",              type = str, required = True,  nargs = "+")
parser.add_argument("-o", "--output", help = "Print correct reads also", action="store_true")
parser.add_argument("-s", "--suffix", help = "Suffix for output files",  type = str, required = False, default = "check.txt")
args = parser.parse_args()

# Define functions
def main( file ):
	count = 0
	with open(file, "r") as fq:
		output=open( "{}.{}".format( file, args.suffix ), "w")
		while True:
			chunck = list( islice( fq, 4) )
			if not chunck:
				sys.stdout.write( "> Done with file {}\n".format( file ) )
				break
			else:
				l1 = chunck[0].strip()
				l2 = chunck[1].strip()
				l3 = chunck[2].strip()
				l4 = chunck[3].strip()
				if( len(re.compile( "^@.+\/[12]$" ).findall(l1) ) == 1 and len(l2) == len(l4) and not "@" in l2 and l3 == "+"):
					count+=4
					if(args.output):
						output.write( "{}".format( "".join(chunck) ) )
					else:
						pass
				else:
					sys.stderr.write( "# !ERROR: file {} after line {}:\n# {}# //\n".format( file, count, "# ".join(chunck) ) )
					output.write(     "# !ERROR: file {} after line {}:\n# {}# //\n".format( file, count, "# ".join(chunck) ) )
					break
	output.close()
	return

# Execute functions
for file in args.input:
	if not os.path.exists(file):
		sys.stderr.write( "! ERROR: Could not find {}\n" )
		pass
	else:
		main( file )

# Quit
exit()
