#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fa_encrypt.py

# Mode 0: Replace sequence ID for an alpha-numeric code. Mode 1: Replace alpha-numeric
# code by original sequence ID.

# The sequence descriptions (full header) must be unique. The codes will be alphanumeric
# strings of up to 16 characters.

# Import modules and libraries
import argparse,re,sys,uuid

# Set arguments
parser=argparse.ArgumentParser()
parser.add_argument("-f","--fasta",help="Path to multifasta file",type=str,required=True)
parser.add_argument("-s","--syns",help="Path to synonyms file (creteade by this program using mode=0)",type=str,required=False)
parser.add_argument("-m","--mode",help="Mode=0 (default): Replace sequence ID for an alpha-numeric code. Mode=1: Replace alpha-numeric code by original sequence ID.",type=int,default="0",choices=[0,1],required=False)
parser.add_argument("-o","--output",help="Prefix for output file names",type=str,default="output",required=False)
parser.add_argument("-v","--verbose",help="Increases output verbosity",action="store_true",default=False,required=False)
args=parser.parse_args()

# Define functions
def parse_multifasta():
	if(args.verbose):
		sys.stdout.write("> Start parsing file ({})\n".format(args.fasta))
	descriptions=[]
	synonyms=[]
	sequences=[]
	handle=open("{}".format(args.fasta),"r")
	for line in handle.readlines():
		line=line.strip()
		if(not line):
			continue
		if(line.startswith(">")):
			description=re.compile(">(.+)").findall(line)[0]
			active_sequence_name=description
			if active_sequence_name not in descriptions:
				descriptions+=[active_sequence_name]
				while True:
					code=re.sub("[^\w]","",str(uuid.uuid1()).upper())[:16]
					if(code not in synonyms):
						synonyms+=[code]
						break
				sequences+=[""]
				if(args.verbose):
					sys.stdout.write("...entry {}\n".format(len(synonyms)))
			continue
		sequences[-1]+=line
	handle.close()
	if(args.verbose):
		sys.stdout.write("> This task is done\n".format(args.fasta))
	return descriptions,synonyms,sequences

def encrypt(synonyms,sequences):
	output_filename="{}_{}".format(args.output,args.fasta)
	if(args.verbose):
		sys.stdout.write("> Expording multifasta file with coded headers to {}\n".format(output_filename))
	handle=open(output_filename,"w")
	for i in range(0,len(synonyms)):
		entry=">{}\n{}\n".format(synonyms[i],sequences[i])
		handle.write(entry)
	handle.close()
	if(args.verbose):
		sys.stdout.write("> This task is done\n".format(output_filename))
	return

def export_synonyms(descriptions,sequences):
	output_filename="{}_synonyms.tsv".format(args.output)
	if(args.verbose):
		sys.stdout.write("> Expording synonyms to {}\n".format(output_filename))
	handle=open(output_filename,"w")
	for i in range(0,len(synonyms)):
		entry="{}\t{}\n".format(synonyms[i],descriptions[i])
		handle.write(entry)
	handle.close()
	if(args.verbose):
		sys.stdout.write("> This task is done\n".format(output_filename))
	return

def parse_synonyms():
	if(args.verbose):
		sys.stdout.write("> Parsing file ({})\n".format(args.syns))
	descriptions=[]
	synonyms=[]
	handle=open(args.syns,"r")
	for line in handle.readlines():
		line=line.strip()
		if(line):
			try:
				i,j=line.split("\t")
				synonyms+=[i]
				descriptions+=[j]
			except:
				pass
	handle.close()
	if(args.verbose):
		sys.stdout.write("> This task is done\n")
	return descriptions,synonyms

def fasta2dic():
	if(args.verbose):
		sys.stdout.write("> Start parsing file ({})\n".format(args.fasta))
	sequences_dic={}
	handle=open("{}".format(args.fasta),"r")
	for line in handle.readlines():
		line=line.strip()
		if(not line):
			continue
		if(line.startswith(">")):
			description=re.compile(">(.+)").findall(line)[0]
			active_sequence_name=description
			if active_sequence_name not in sequences_dic:
				sequences_dic[active_sequence_name]=""
				if(args.verbose):
					sys.stdout.write("...entry {}\n".format(len(synonyms)))
			continue
		sequences_dic[active_sequence_name]+=line
	handle.close()
	if(args.verbose):
		sys.stdout.write("> This task is done\n".format(args.fasta))
	return sequences_dic

def decrypt(descriptions,synonyms,sequences_dic):
	output_filename="{}_{}".format(args.output,args.fasta)
	if(args.verbose):
		sys.stdout.write("> Printing multifasta with decoded headers to {}\n".format(output_filename))
	handle=open(output_filename,"w")
	for n in range(0,len(synonyms)):
		a=synonyms[n]
		b=descriptions[n]
		s=sequences_dic[a]
		entry=">{}\n{}\n".format(b,s)
		handle.write(entry)
	handle.close()
	if(args.verbose):
		sys.stdout.write("> This task is done\n".format(args.fasta))
	return

# Execute functions
if(args.mode==0):
	descriptions,synonyms,sequences=parse_multifasta()
	encrypt(synonyms,sequences)
	export_synonyms(descriptions,sequences)
elif(args.mode==1):
	if(args.syns):
		descriptions,synonyms=parse_synonyms()
		sequences_dic=fasta2dic()
		for i in synonyms:
			if not i in sequences_dic:
				sys.stderr.write("¡ERROR: code {} not found in {}!\n".format(i,args.fasta))
				exit()
		decrypt(descriptions,synonyms,sequences_dic)
	else:
		sys.stderr.write("¡ERROR: For decryption mode (-m 1) the path to a tab-separated synonyms file must be provided (-s <path>)!\n")
		exit()

if(args.verbose):
	sys.stdout.write("> All tasks are done\n")

# Quit
exit()
