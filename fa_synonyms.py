#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fa_synonyms.py
# Read synonyms table, find and replace names in multifasta file.

# Import modules and libraries
import argparse,re,sys

# Set arguments
parser=argparse.ArgumentParser()
parser.add_argument("-f","--fasta",help="(Multi)fasta file",type=str,required=True)
parser.add_argument("-s","--synonyms",help="Synonyms file (tab-separated values)",type=str,required=True)
parser.add_argument("-m","--mode",help="1: left <-- right [default]; 2: left --> right",type=int,choices=range(1,3),default=1,required=False)
args=parser.parse_args()

# Define functions
def main():
	d={}
	handle=open("{}".format(args.synonyms),"r")
	for l in handle.readlines():
		l=l.strip()
		l=re.sub("#.*","",l)
		if(l):
			x,y=l.split("\t")
			x=x.strip()
			y=y.strip()
			if(x in d):
				sys.stderr.write("! ERROR parsing synonyms.\n\tMake sure synonyms are unique and that the columns are tab-separeted\n")
				exit()
			d[x]=y
	handle.close()
	if(args.mode==1):
		handle=open("{}".format(args.fasta),"r")
		fasta=handle.read()
		handle.close()
		for k in sorted(d, key=lambda k: len(d[k]), reverse=True):
			sys.stderr.write("-- {} : {}\n".format(k,d[k]))
			fnd=d[k]
			rpl=k
			fasta=fasta.replace(fnd,rpl)
		sys.stdout.write("{}".format(fasta))
	else:
		handle=open("{}".format(args.fasta),"r")
		fasta=handle.read()
		handle.close()
		for k in sorted(d, key=lambda k: len(k), reverse=True):
			sys.stderr.write("-- {} : {}\n".format(k,d[k]))
			fnd=k
			rpl=d[k]
			fasta=fasta.replace(fnd,rpl)
		sys.stdout.write("{}".format(fasta))
	return


# Execute functions
main()


# Quit
exit()
