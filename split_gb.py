#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# split_gb.py
# Split a genbank sequence file in one file per entry

# Import modules and libraries
import argparse,re

# Set arguments
parser=argparse.ArgumentParser()
parser.add_argument("-i","--input",help="Input",type=str,required=True)
args=parser.parse_args()

# Define functions
def main():
	handle=open(args.input,"r")
	all=re.compile("(.+?//\s+)",re.I|re.M|re.S).findall(handle.read())
	handle.close()
	for entry in all:
		id=re.compile("ACCESSION\s+(.+)\s*").findall(entry)[0]
		handle=open("{}.gb".format(id),"w")
		handle.write(entry)
		handle.close()
	return


# Execute functions
main()


# Quit
exit()
